package com.watcharaphon.midterm;

import java.util.Random;
import java.util.Scanner;

public class PlayerApp {
// --------------Method----------------
    static int askPlayer() {
        Scanner sc = new Scanner(System.in);
        System.out.print("How many players ?  2 or 3 ? = ");
        int numplayer = sc.nextInt();
        return numplayer;
    }

    static void checkScoreP2(int HPP2) {
        if (HPP2 <= 0) {
            System.out.println("Player 1 is the winner!!");
            System.exit(0);
        }
    }

    static void checkScoreP1(int HPP1) {
        if (HPP1 <= 0) {
            System.out.println("Player 2 is the winner!!");
            System.exit(0);
        }
    }

    static void checkScoreP1In3(int HPP1, int HPP2, int HPP3) {
        if(HPP1 <= 0 && HPP2>HPP3) {
            System.out.println("Player 2 is the winner!!");
            System.exit(0);
        } else if(HPP1 <= 0 && HPP2<HPP3) {
            System.out.println("Player 3 is the winner!!");
            System.exit(0);
        } else if(HPP1 <= 0 && HPP2==HPP3) {
            System.out.println("No winner!!");
            System.exit(0);
        }
    }

    static void checkScoreP2In3(int HPP1, int HPP2, int HPP3) {
        if(HPP2 <= 0 && HPP1>HPP3) {
            System.out.println("Player 1 is the winner!!");
            System.exit(0);
        } else if(HPP2 <= 0 && HPP1<HPP3) {
            System.out.println("Player 3 is the winner!!");
            System.exit(0);
        } else if(HPP2 <= 0 && HPP1==HPP3) {
            System.out.println("No winner!!");
            System.exit(0);
        }
    }

    static void checkScoreP3In3(int HPP1, int HPP2, int HPP3) {
        if(HPP3 <= 0 && HPP1>HPP2) {
            System.out.println("Player 1 is the winner!!");
            System.exit(0);
        } else if(HPP3 <= 0 && HPP1<HPP2) {
            System.out.println("Player 2 is the winner!!");
            System.exit(0);
        } else if(HPP3 <= 0 && HPP1==HPP2) {
            System.out.println("No winner!!");
            System.exit(0);
        }
    }
    //------------------------------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            int numplayer = askPlayer();

            if (numplayer == 2) {
                Player player1 = new Player(20, "P1");
                player1.print();
                Player player2 = new Player(20, "P2");
                player2.print();
                while (true) {
                    int random = (int) (Math.random() * 10);
                    System.out.println("----------");
                    System.out.print("P1 = ");
                    int score1 = sc.nextInt();
                    System.out.print("P2 = ");
                    int score2 = sc.nextInt();
                    int d1 = Math.abs(random - score1);
                    int d2 = Math.abs(random - score2);
                    if (d1 > d2) {
                        System.out.println("Random is " + random);
                        player2.loseGame(random);
                        player1.print();
                        player2.print();
                        int HPP2 = player2.getHP();
                        int HPP1 = player1.getHP();
                        checkScoreP2(HPP2);
                        checkScoreP1(HPP1);
                    } else if (d1 < d2) {
                        System.out.println("Random is " + random);
                        player1.loseGame(random);
                        player1.print();
                        player2.print();
                        int HPP2 = player2.getHP();
                        int HPP1 = player1.getHP();
                        checkScoreP2(HPP2);
                        checkScoreP1(HPP1);
                    } else {
                        System.out.println("Random is " + random);
                        System.out.println("Equal");
                    }

                }
            }

            else if (numplayer == 3) {
                System.out.println("----------");
                Player player1 = new Player(20, "P1");
                player1.print();
                Player player2 = new Player(20, "P2");
                player2.print();
                Player player3 = new Player(20, "P3");
                player3.print();
                while (true) {
                    int random = (int) (Math.random() * 10);
                    System.out.print("P1 = ");
                    int score1 = sc.nextInt();
                    System.out.print("P2 = ");
                    int score2 = sc.nextInt();
                    System.out.print("P3 = ");
                    int score3 = sc.nextInt();
                    int d1 = Math.abs(random - score1);
                    int d2 = Math.abs(random - score2);
                    int d3 = Math.abs(random - score3);
                    if (d1 < d2 && d1 < d3) {
                        System.out.println("Random is " + random);
                        player1.loseGame(random);
                        player1.print();
                        player2.print();
                        player3.print();
                        int HPP2 = player2.getHP();
                        int HPP1 = player1.getHP();
                        int HPP3 = player3.getHP();
                        checkScoreP1In3(HPP1, HPP2, HPP3);
                    } else if (d2 < d1 && d2 < d3) {
                        System.out.println("Random is " + random);
                        player2.loseGame(random);
                        player1.print();
                        player2.print();
                        player3.print();
                        int HPP2 = player2.getHP();
                        int HPP1 = player1.getHP();
                        int HPP3 = player3.getHP();
                        checkScoreP2In3(HPP1, HPP2, HPP3);
                    } else if (d3 < d1 && d3 < d2) {
                        System.out.println("Random is " + random);
                        player3.loseGame(random);
                        player1.print();
                        player2.print();
                        player3.print();
                        int HPP2 = player2.getHP();
                        int HPP1 = player1.getHP();
                        int HPP3 = player3.getHP();
                        checkScoreP3In3(HPP1, HPP2, HPP3);
                    } else {
                        System.out.println("Random is " + random);
                        System.out.println("Equal");
                    }
                }
            }
        }
    }
}
