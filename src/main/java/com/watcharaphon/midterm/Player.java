package com.watcharaphon.midterm;

public class Player {
    private int HP;
    private String symbol;

    public Player(int HP, String symbol) { //constructor
        this.HP = HP;
        this.symbol = symbol;
    }

    // Getter & Setter
    public int getHP() {
        return HP;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setHP() {
        this.HP = HP;
    }

    // Method
    public int loseGame(int random) {
        HP = HP - random;
        return HP;
    }

    public void print() {
        System.out.println(symbol + " HP = " + HP );
    }
}
