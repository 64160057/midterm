package com.watcharaphon.midterm;

import java.util.Scanner;

public class StockApp {
    //--------Method------------
    static void printWelcome() {
        System.out.println("*---------Welcome---------*");
    }

    static void printMenu() {
        System.out.println("*-------------------------*");
        System.out.println("- Add stock put 1");
        System.out.println("- Release stock put 2");
        System.out.println("- Check Stock put 3");
        System.out.println("- Exit put 4");
    }

    static void printAddProduct() {
        System.out.println("-------------------------");
        System.out.println("*---Add Stock---*");
        System.out.println("1. Hammer");
        System.out.println("2. Screwdriver");
        System.out.println("3. Aluminuim");
        System.out.println("4. Drill");
        System.out.println("5. Riveter");
        System.out.println("6. Rivets");
        System.out.println("7. Back to Menu");
    }

    static void printReleaseProduct() {
        System.out.println("-------------------------");
        System.out.println("*---Release Stock---*");
        System.out.println("1. Hammer");
        System.out.println("2. Screwdriver");
        System.out.println("3. Aluminuim");
        System.out.println("4. Drill");
        System.out.println("5. Riveter");
        System.out.println("6. Rivets");
        System.out.println("7. Back to Menu");
    }

    static int asknum() {
        Scanner sc = new Scanner(System.in);
        System.out.print("amount ? = ");
        int amount = sc.nextInt();
        return amount;
    }

//--------------------------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Stock hammer = new Stock("Hammer", 0);
        Stock screwdriver = new Stock("Screwdriver");
        Stock aluminium = new Stock("Aluminium", 20);
        Stock drill = new Stock("Drill", 10);
        Stock riveter = new Stock("Riveter");
        Stock rivets = new Stock("Rivets");

        printWelcome();
        while (true) {
            printMenu();
            System.out.print("  put = ");
            int menu = sc.nextInt();
            switch (menu) {
                case 1:
                    while (true) {
                        printAddProduct();
                        System.out.print("Put = ");
                        int chpd = sc.nextInt();
                        if (chpd == 1) {
                            int amount = asknum();
                            hammer.addStock(amount);
                            hammer.print();
                        } else if (chpd == 2) {
                            int amount = asknum();
                            screwdriver.addStock(amount);
                            screwdriver.print();
                        } else if (chpd == 3) {
                            int amount = asknum();
                            aluminium.addStock(amount);
                            aluminium.print();
                        } else if (chpd == 4) {
                            int amount = asknum();
                            drill.addStock(amount);
                            drill.print();
                        } else if (chpd == 5) {
                            int amount = asknum();
                            riveter.addStock(amount);
                            riveter.print();
                        } else if (chpd == 6) {
                            int amount = asknum();
                            rivets.addStock(amount);
                            rivets.print();
                        } else if (chpd == 7) {
                            break;
                        } else {
                            System.out.println("*-----Pleas try again-----*");
                        }
                    }
                    break;
                case 2:
                    while (true) {
                        printReleaseProduct();
                        System.out.print("Put = ");
                        int chpd2 = sc.nextInt();
                        if (chpd2 == 1) {
                            int amount = asknum();
                            hammer.releaseStock(amount);
                            hammer.print();
                        } else if (chpd2 == 2) {
                            int amount = asknum();
                            screwdriver.releaseStock(amount);
                            screwdriver.print();
                        } else if (chpd2 == 3) {
                            int amount = asknum();
                            aluminium.releaseStock(amount);
                            aluminium.print();
                        } else if (chpd2 == 4) {
                            int amount = asknum();
                            drill.releaseStock(amount);
                            drill.print();
                        } else if (chpd2 == 5) {
                            int amount = asknum();
                            riveter.releaseStock(amount);
                            riveter.print();
                        } else if (chpd2 == 6) {
                            int amount = asknum();
                            rivets.releaseStock(amount);
                            rivets.print();
                        } else if (chpd2 == 7) {
                            break;
                        } else {
                            System.out.println("*-----Pleas try again-----*");
                        }
                    }
                    break;
                case 3:
                    int sumall = hammer.getStock() + screwdriver.getStock() + aluminium.getStock() + drill.getStock() + riveter.getStock() + rivets.getStock();
                    System.out.println("------------------------------------------");
                    System.out.println("|          Name          |    stock      |");
                    System.out.println("------------------------------------------");
                    System.out.println("| Hammer                 |      " + hammer.getStock());
                    System.out.println("-----------------------------------------|");
                    System.out.println("| Scewdriver             |      " + screwdriver.getStock());
                    System.out.println("-----------------------------------------|");
                    System.out.println("| Aliminium              |      " + aluminium.getStock());
                    System.out.println("-----------------------------------------|");
                    System.out.println("| Drill                  |      " + drill.getStock());
                    System.out.println("-----------------------------------------|");
                    System.out.println("| Riveter                |      " + riveter.getStock());
                    System.out.println("-----------------------------------------|");
                    System.out.println("| Rivets                 |      " + rivets.getStock());
                    System.out.println("-----------------------------------------|");
                    System.out.println("| Sum                    |      " + sumall);
                    System.out.println("------------------------------------------");
                    System.out.print("put 1 : Back to Menu : ");
                    int exit = sc.nextInt();
                    if(exit == 1) break;
                    break;
                case 4:
                    System.out.println("  Good Bye!!");
                    System.exit(0);
                default:
                    break;
            }
        }
    }

}
