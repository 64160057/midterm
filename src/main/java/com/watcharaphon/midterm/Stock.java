package com.watcharaphon.midterm;

public class Stock {
    private String name;
    private int stock;

    public Stock(String name, int stock) {
        this.name = name;
        this.stock = stock;
    }

    public Stock(String name) { // Constructor Overloading
        this(name, 0);
    }
    
    //Getter
    public String getName() {
        return name;
    }

    public int getStock() {
        return stock;
    }

    //Medthod
    public boolean addStock(int amount) {
        if (amount>0) {
            stock = stock + amount;
            return true;
        }
        return false;
    }

    public boolean releaseStock(int amount) {
        if(amount<=stock) {
            stock = stock - amount;
            return true;
        } else {
            System.out.println("Sorry, " + name + " not enough");
            return false;
        }
    }

    public void print() {
        System.out.println("Product : " + name + " Stock = " + stock);
    }
}
